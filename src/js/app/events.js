import Utils from './utils';

export default

class Events
{
    listenFormSubmit(formId, event)
    {
        let form = document.getElementById(formId);

        if(!form) {
            console.warn(`Unable to find form with id "${formId}"`);
            return;
        }

        form.addEventListener('submit', function(e) {
            e.preventDefault();

            ga('send', 'event', event.category, event.action, event.label, {
                hitCallback: Utils.createFunctionWithTimeout(() => {
                    // console.info(`Event from #${formId} sent!`);
                    form.submit();
                })
            });
        });
    }

    /**
     * This is fired when user submit from top header
     *
     * @type: Registration
     */
    hero()
    {
        this.listenFormSubmit('hero-form', {
            category: 'Sign Up',
            action: 'fast-hero',
        });
    }

    /**
     * This is fired when user sign up
     * from cta in the page bottom
     *
     * @type: Registration
     */
    editorForm()
    {
        this.listenFormSubmit('editor-form', {
            category: 'Sign Up',
            action: 'fast-cta::editor'
        });
    }

    /**
     * This is fired when user sign up
     * from cta in the page bottom
     *
     * @type: Registration
     */
    finalForm()
    {
        this.listenFormSubmit('final-form', {
            category: 'Sign Up',
            action: 'fast-cta::bottom'
        });
    }
}