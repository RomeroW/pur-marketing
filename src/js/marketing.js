import App from './marketing/app';

/*
 * Create application instance
 */
let app = new App;

app.whenDocumentReady(() => {
    app.setupVueApp();
    app.setupGoogleAnalyticEvents();
});