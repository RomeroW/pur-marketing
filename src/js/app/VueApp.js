export default {
    el: '#app',
    data: {
        scheduleView: {
            current: 'days'
        },
        process: {
            current: 'projects'
        },
        analytic: {
            current: 'fast',
            features: {
                fast: {
                    title: 'Быстрая статистика',
                    description: 'Основные метрики по всему проекту и для каждой страницы. Показаны текущие значения и их изменения за выбранный период.',
                    image: '/img/marketing/quick-stats.png',
                },
                audience: {
                    title: 'Анализ аудитории',
                    description: 'Показываем прирост аудитории и количество подписчиков. Все показатели пересчитываются один раз в час.',
                    image: '/img/marketing/analytic-audience.png',
                },
                engagement: {
                    title: 'Вовлеченность постов',
                    description: '',
                    image: '/img/marketing/analytic-engagement.png',
                },
                bestTime: {
                    title: 'Лучшее время для поста',
                    description: 'Проанализировав ваши данные, мы покажем, когда лучше всего публиковать посты.',
                    image: '/img/marketing/analytic-best-time.png',
                },
                posts: {
                    title: 'Анализ постов',
                    description: 'Смотрите как люди взаимодействуют с записями. Это поможет понять, какой контент для какой соцсети давать.',
                    image: '/img/marketing/analytic-posts.png',
                },
            },
        }
    }
}