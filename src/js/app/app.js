import Utils from './utils';
import Events from './events';
import Scroll from './scroll';
import VueApp from "./VueApp";

export default class App
{
    isMobile;
    events;
    scroll;

    constructor()
    {
        this.isMobile = Utils.isMobile();
        this.events = new Events;
        this.scroll = new Scroll;
    }

    whenDocumentReady(callback)
    {
        document.onreadystatechange = () => {
            if (document.readyState === 'interactive') {
                callback();
            }
        };
    }

    setupVueApp()
    {
        new Vue(VueApp);
    }

    setupGoogleAnalyticEvents()
    {
        this.events.hero();
        this.events.editorForm();
        this.events.finalForm();
    }

    setupScrollListeners()
    {
        window.addEventListener('scroll', () => {
            this.scroll.hit();
        });
    }
}